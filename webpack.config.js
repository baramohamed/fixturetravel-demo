var Encore = require('@symfony/webpack-encore');

Encore
    .setOutputPath('public/build/')
    .setPublicPath('/build')
    .cleanupOutputBeforeBuild()
    .autoProvidejQuery()
    .autoProvideVariables({
        "window.Bloodhound": require.resolve('bloodhound-js'),
        "jQuery.tagsinput": "bootstrap-tagsinput"
    })
    .enableSassLoader()

    //---------------  My actual js and stylesheets ---------------------

    .addEntry('js/myapp',['./assets/js/resume.js'])
    .addStyleEntry('css/myapp', ['./assets/scss/resume.scss','./assets/fontawesome/scss/all.scss'])
    
    //---------------  Copy my photo to public directory ---------------------
    .copyFiles({
        from: './assets/img/',
        to: 'images/[path][name].[ext]',
    })

    //---------------  Symfony demo js files removed because not needed ---------------------

    //.addEntry('js/app', './assets/js/app.js')
    //.addEntry('js/login', './assets/js/login.js')
    //.addEntry('js/admin', './assets/js/admin.js')
    //.addEntry('js/search', './assets/js/search.js')

    //---------------  Symfony demo stylesheets removed because not needed ---------------------

    //.addStyleEntry('css/app', ['./assets/scss/app.scss'])
    //.addStyleEntry('css/admin', ['./assets/scss/admin.scss'])

    .splitEntryChunks()
    .enableSingleRuntimeChunk()    
;

module.exports = Encore.getWebpackConfig();
