Fixture Travel Demo
========================

Requirements
------------

  * VirtualBox 5.2.0;
  * Vagrant 2.2.3;
  * Homestead Vagrant Box 7.1.0;

Set-up & Configuration
----------------------

Install VirtualBox: https://www.virtualbox.org/wiki/Downloads

Install Vagrant: https://www.vagrantup.com/downloads.html

Install The Homestead Vagrant Box - command from terminal -: vagrant box add laravel/homestead

Clone Git Repo: git clone https://gitlab.com/baramohamed/fixturetravel-demo.git

Edit The Hosts File - '/etc/hosts' on Mac and Linux, 'C:\Windows\System32\drivers\etc\hosts' on Windows -: add '192.168.10.10  fixturetraveldemo.com' to the end of file


Launching The Vagrant Box & Symfony App
---------------------------------------

Get into the project directory - command from terminal -: cd fixturetravel-demo

Launch The Vagrant Box - command from terminal -: vagrant up

Vagrant will boot the virtual machine and automatically make necessary configuration

Type 'fixturetraveldemo.com' in your browser and you will be able to access the site (it may take some time for the virtual machine to launch the web server)